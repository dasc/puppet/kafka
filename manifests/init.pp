class kafka (
    String $installdir,
    Integer $broker_id,
    String $storage_dir,
    String $ssl_keystore_location,
    String $ssl_truststore_location,
    String $ssl_key_password,
    String $ssl_truststore_password,
    String $ssl_keystore_password,
    Boolean $use_ssl,
    Optional[String] $producer_config = undef,
    Optional[String] $consumer_config = undef,
    Optional[String] $config_source = undef,
    String $metrics_config_source,
    Hash[String, Struct[{id => Integer,
                         Optional[clientPort] => Integer,
                         Optional[peerPort] => Integer,
                         Optional[electionPort] => Integer}]] $zookeepers = {},
) {
    $zooDefaultClientPort = lookup('hadoop::zookeeper::defaultClientPort')
    $zookeeper_csv = $zookeepers.reduce([]) |$memo, $server| {
        concat($memo, join([$server[0], $server[1]['clientPort']?{Integer => $server[1]['clientPort'], default => $zooDefaultClientPort}], ':'))
    }

    package {'kafka': ensure => installed, }

    service {'kafka':
        require => Package['kafka'],
    }

    file {"$installdir/libs/jmx_prometheus_javaagent-0.3.1.jar":
        show_diff => false,
        ensure => 'file',
        owner => 'kafka',
        group => 'kafka',
        mode => '0644',
        source => 'puppet://puppet/modules/kafka/jmx_prometheus_javaagent-0.3.1.jar',
        require => Package['kafka'],
        notify => Service['kafka'],
    }

    file {'/etc/kafka/kafka.yml':
        ensure => 'file',
        owner => 'kafka',
        group => 'kafka',
        mode => '0644',
        source => "${metrics_config_source}",
        notify => Service['kafka'],
    }

    if $producer_config {
        file {'/etc/kafka/producer.properties':
            ensure => 'file',
            owner => 'kafka',
            group => 'kafka',
            mode => '0644',
            source => "${producer_config}",
            require => Package['kafka'],
            notify => Service['kafka'],
        }
    }

    if $consumer_config {
        file {'/etc/kafka/consumer.properties':
            ensure => 'file',
            owner => 'kafka',
            group => 'kafka',
            mode => '0644',
            source => "${consumer_config}",
            require => Package['kafka'],
            notify => Service['kafka'],
        }
    }

    if $config_source {
        file {'/etc/kafka/server.properties':
            ensure => 'file',
            owner => 'kafka',
            group => 'kafka',
            mode => '0644',
            source => "${config_source}",
            require => Package['kafka'],
            notify => Service['kafka'],
        }
    } else {
        file {'/etc/kafka/server.properties':
            ensure => 'file',
            owner => 'kafka',
            group => 'kafka',
            mode => '0644',
            content => epp('kafka/server.properties.epp', {
                'broker_id' => $broker_id,
                'zookeeper_csv' => $zookeeper_csv,
                'storage_dir' => $storage_dir,
                'ssl_keystore_location' => $ssl_keystore_location,
                'ssl_truststore_location' => $ssl_truststore_location,
                'ssl_key_password' => $ssl_key_password,
                'ssl_truststore_password' => $ssl_truststore_password,
                'ssl_keystore_password' => $ssl_keystore_password,
                'use_ssl' => $use_ssl,
            }),
            require => Package['kafka'],
            notify => Service['kafka'],
        }
    }

    exec {"kafka daemon-reload":
        command => '/usr/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service <| |>

    file {'/usr/lib/systemd/system/mirrormaker@.service':
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        content => epp('kafka/mirrormaker@.service.epp', {
            'installdir' => $installdir,
        }),
        notify => Exec['kafka daemon-reload'],
    }
}
