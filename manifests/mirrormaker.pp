define kafka::mirrormaker (
    String $consumer_config_source,
    String $producer_config_source,
    String $metrics_config_source,
    Array[String] $bootstrap_servers,
    String $group_id,
    Array[String] $whitelist,
    Integer $num_streams,
    Integer $jmxPort,
    Integer $metricsPort,
) {
    include ::kafka

    file {"/etc/sysconfig/mirrormaker-${title}":
        ensure => 'file',
        owner => 'root',
        group => 'root',
        mode => '0644',
        #source => "${sysconfig_source}",
        content => epp('kafka/sysconfig.mirrormaker', {
            installdir => $::kafka::installdir,
            metrics_config => "/etc/kafka/mirrormaker-${title}.yml",
            metricsPort => $metricsPort,
            producer_config => "/etc/kafka/producer-${title}.properties",
            consumer_config => "/etc/kafka/consumer-${title}.properties",
            jmxPort => $jmxPort,
            streams => $num_streams,
            whitelist => $whitelist,
        }),
        require => Package['kafka'],
        notify => Service["mirrormaker@${title}"],
    }

    service {"mirrormaker@${title}":
        ensure => 'running',
        enable => 'true',
        subscribe => File['/usr/lib/systemd/system/mirrormaker@.service', "/etc/sysconfig/mirrormaker-${title}"],
    }

    file {"/etc/kafka/producer-${title}.properties":
        ensure => 'file',
        owner => 'kafka',
        group => 'kafka',
        mode => '0644',
        source => "${producer_config_source}",
        require => Package['kafka'],
        notify => Service["mirrormaker@${title}"],
    }

    file {"/etc/kafka/consumer-${title}.properties":
        ensure => 'file',
        owner => 'kafka',
        group => 'kafka',
        mode => '0644',
        source => "${consumer_config_source}",
        require => Package['kafka'],
        notify => Service["mirrormaker@${title}"],
    }

    file {"/etc/kafka/mirrormaker-${title}.yml":
        ensure => 'file',
        owner => 'kafka',
        group => 'kafka',
        mode => '0644',
        source => "${metrics_config_source}",
        require => Package['kafka'],
        notify => Service["mirrormaker@${title}"],
    }
}
